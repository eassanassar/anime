# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table episode (
  id                        bigint auto_increment not null,
  serie_id                  bigint,
  episode                   integer,
  season                    integer,
  constraint pk_episode primary key (id))
;

create table genre (
  id                        integer auto_increment not null,
  name                      varchar(255),
  constraint pk_genre primary key (id))
;

create table links (
  id                        integer auto_increment not null,
  episode_id                bigint,
  link                      varchar(255),
  dead                      tinyint(1) default 0,
  approved                  tinyint(1) default 0,
  constraint pk_links primary key (id))
;

create table serie (
  id                        bigint auto_increment not null,
  name                      varchar(128),
  summery                   varchar(512),
  released                  datetime(6),
  status                    varchar(255),
  updated                   datetime(6) not null,
  created                   datetime(6) not null,
  constraint pk_serie primary key (id))
;

create table user (
  userid                    bigint auto_increment not null,
  roleid                    integer,
  username                  varchar(64),
  email                     varchar(64) not null,
  password                  varchar(128),
  lastlogin                 datetime(6),
  deleted                   tinyint(1) default 0,
  updated                   datetime(6) not null,
  created                   datetime(6) not null,
  constraint pk_user primary key (userid))
;

create table anime_genre (
  id                        bigint auto_increment not null,
  serie_id                  bigint,
  genre_id                  integer,
  constraint pk_anime_genre primary key (id))
;

alter table episode add constraint fk_episode_serie_1 foreign key (serie_id) references serie (id) on delete restrict on update restrict;
create index ix_episode_serie_1 on episode (serie_id);
alter table links add constraint fk_links_episode_2 foreign key (episode_id) references episode (id) on delete restrict on update restrict;
create index ix_links_episode_2 on links (episode_id);
alter table anime_genre add constraint fk_anime_genre_serie_3 foreign key (serie_id) references serie (id) on delete restrict on update restrict;
create index ix_anime_genre_serie_3 on anime_genre (serie_id);
alter table anime_genre add constraint fk_anime_genre_genre_4 foreign key (genre_id) references genre (id) on delete restrict on update restrict;
create index ix_anime_genre_genre_4 on anime_genre (genre_id);


create index ix_user_username_5 on user(username);
create index ix_user_email_6 on user(email);

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table episode;

drop table genre;

drop table links;

drop table serie;

drop table user;

drop table anime_genre;

SET FOREIGN_KEY_CHECKS=1;

