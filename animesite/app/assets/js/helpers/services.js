define([ 'app' ], function(app) {
	app.factory('Episode', ['$resource', function($resource) {
		return $resource('/api/episode/:act', null,{
			'create': { method:'POST' }
		});
	}]);
	
	app.factory('Serie', ['$resource', function($resource) {
		return $resource('/api/serie/:act/:id', null,{
			'create': { method:'POST' },
			'update': { method:'POST' }
		});
	}]);
	
	app.factory('Auth', ['$resource', function($resource) {
		return $resource('/api/auth/:act', null,{
			'create': { method:'POST' },
		});
	}]);
});
