define([ 'angular', 'ui-bootstrap', 'angular-resource'], function(angular) {
	app = angular.module(App, ['ui.bootstrap', 'ngResource']);

	return app;
});
