var App = 'AnimeApp';
require.config({
	paths: {
		angular: './lib/angular.min',
		'angular-resource': './lib/angular-resource.min',
		'ui-bootstrap': './lib/ui-bootstrap-tpls-1.2.5.min',
	
	},
	shim: {
		angular: {
			exports: 'angular'
		},
		'angular-resource': {
			deps: ['angular']
		},
		'ui-bootstrap': {
			deps: ['angular']
		}
	}
});
//Now we're ready to require JointJS and write our application code.
require([
         'app',
         'controllers/mainCtrl',
         'controllers/authCtrl',
         'helpers/services'], function() {
	angular.bootstrap(document, [ App ]);
});