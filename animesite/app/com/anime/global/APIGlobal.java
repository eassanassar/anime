package com.anime.global;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.databind.node.ObjectNode;

import models.login.User;
import play.mvc.Controller;
import play.mvc.Result;
public class APIGlobal extends Controller {
	
	public static final String STATUS = "status";
	public static final String TITLE = "title";
	public static final String MSG = "msg";
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	
	

	public static final String ERROR_INPUT = "INPUT";
	public static final String ERROR_UNKNOWN = "UNKNOWN";
	public static final String ERROR_API = "APIERR";
	
	public static Result response(boolean success, String title, String message, ObjectNode result, String errorType) {
		result.put(TITLE, title);
		result.put(MSG, message);
		
		if (!success) {
			String currentIp = request().remoteAddress();
			
			result.put(STATUS, ERROR);
			
			play.Logger.warn("Json Response to '"+currentIp+": '"+title+" - "+result.toString());
			return badRequest(result);
		}
		else {
			result.put(STATUS, SUCCESS);
			return ok(result);
		}
	}
	
	/*public User AddUserLogin(User loggedUser, boolean saveObject) throws Exception {
		//Check here to see if the user already logged in from diffrent IP Address
		
					
			String currentIp = request().remoteAddress();
			
			//We have a session open already
			UserSession.updateLastActivity(loggedUser.getId(), getSessionId(), currentIp, userAgent);
			
			loggedUser.setLastlogin(new Timestamp(new Date().getTime()));
			
			if (saveObject || loggedUser.getLastlogin() == null) {
				loggedUser.save();
			}

			return loggedUser;
		
	}
	*/
	

}
