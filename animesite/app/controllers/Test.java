package controllers;

import java.util.List;

import com.anime.global.APIGlobal;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Episode;
import models.Serie;
import play.mvc.Controller;
import play.libs.Json;
import play.mvc.*;

public class Test extends Controller{

	public Result Test1() {

		Serie series = new Serie();
		Episode ep = new Episode();

		ep.setEpisode(1);
		ep.setSeason(1);

		series.setName("TEST 1");
		series.setStatus("ongoing");
		series.setReleased(null);
		series.addEpisode(ep);
		series.save();
		
		
		ObjectNode result = Json.newObject();
		//JsonNode jsonNode = Json.toJson(bg);
		result.put("test","test Works");
		series = Serie.getbyid(1);
		List<Episode> eps = series.getEpisodesList();
		JsonNode jsonNode = Json.toJson(eps);
		result.set("js", Json.toJson(jsonNode));
		return APIGlobal.response(true, "test", "Episode was Created successfuly", result, APIGlobal.SUCCESS);


	}

}
