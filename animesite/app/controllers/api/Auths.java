package controllers.api;

import com.anime.global.APIGlobal;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.Passwords;
import models.login.User;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class Auths extends Controller {
	String title ="auth";
/*	String title = "Episodes";
	public Result episode_create(){
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		if (json == null) {
			play.Logger.error("Episode - episode_create - Expecting Json data!");
			return APIGlobal.response(false, title, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
			//return APIGlobal.response(false, NAME, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
		}

		result.put("test","test Works");
		result.set("js", json);
		return APIGlobal.response(true, title, "Episode was Created successfuly", result, APIGlobal.SUCCESS);
	}
*/
	public Result login(){
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		
		if (json.isNull()){
			play.Logger.error("Auth- Register  - Expecting Json data!");
			return APIGlobal.response(false, title, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
			//return APIGlobal.response(false, NAME, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
		}else{
			String email = json.get("email").asText();
			String password = json.get("password").asText();
			//String currentIp = request().remoteAddress();
		
			if (email == null || password == null) {
				return badRequest("Missing arguments!");
			}
			
			//TODO: check loginm attemts 
			
			User loggedUser = User.getuserbyemail(email);
			//if user is registered 
			if(loggedUser != null){
				//if password is correct
				if(Passwords.checkPassword(password, loggedUser.getPassword())){
					result.set("user", Json.toJson(loggedUser));
					 // email/password OK, so now we set the session variable and only go to authenticated pages.
				    session().clear();
				    session("email", email);
					return APIGlobal.response(true, title, "Log in Success", result, APIGlobal.SUCCESS);
				}else{
					return APIGlobal.response(false, title, "Email or password are incorect", result, APIGlobal.ERROR_INPUT);
				}
			}else{
				return APIGlobal.response(false, title, "user does not exist", result, APIGlobal.ERROR_INPUT);
			}
			
		}
	//	result.put("test","test Works");
	//result.set("js", json);
		//result.set("user", Json.toJson(user));
	//	return APIGlobal.response(false, title, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
	}
	
	public Result createEdit() throws Exception{
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		User user = new User();
		if (json == null) {
			play.Logger.error("Auth- Register  - Expecting Json data!");
			return APIGlobal.response(false, title, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
			//return APIGlobal.response(false, NAME, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
		}else{
			String email = json.get("email").asText();
		//	Logger.debug("--------------------------------------- "+email+" ----------------------");
			try{
				if(!email.isEmpty()){
					if(User.is_email_available(email)){
						user = User.createEdit(json, true);
					}else{
						return APIGlobal.response(false, title, "Email not Available", result, APIGlobal.ERROR_INPUT);
					}
				}else{
					return APIGlobal.response(false, title, "Email can not be empty", result, APIGlobal.ERROR_INPUT);
				}
			}catch(Exception e){
				return APIGlobal.response(false, title, "An Error Accourd while Regestring", result, APIGlobal.ERROR_UNKNOWN);
			}
			
		}

		//result.put("test","test Works");
		//result.set("js", json);
		//result.set("user", Json.toJson(user));
		return APIGlobal.response(true, title, "User was Registered successfuly", result, APIGlobal.SUCCESS);
	}
	

}


