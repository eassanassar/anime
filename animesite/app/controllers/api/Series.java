package controllers.api;

import com.anime.global.APIGlobal;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Serie;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class Series extends Controller {
	String title= "Series";
	
	
	public Result serie_create(){
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		if (json == null) {
			play.Logger.error("Series - serie_create - Expecting Json data!");
			return APIGlobal.response(false, title, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
			//return APIGlobal.response(false, NAME, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
		}else{
			ObjectNode object = (ObjectNode) json;
			
			if (!object.hasNonNull("name")) {
				return APIGlobal.response(true, title, "series_create , name can't be empty ", result, APIGlobal.SUCCESS);
			}
			else if (!object.hasNonNull("summery")) {
				return APIGlobal.response(true, title, "series_create , name can't be empty ", result, APIGlobal.SUCCESS);
			}
			else if(!Serie.is_name_available(object.get("name").asText())) {
				return APIGlobal.response(false, title, "Type Name not available!", result, APIGlobal.ERROR_INPUT);
			}else{
				try {
				    Serie created = Serie.addSerie(object);
				    				    
				    //TODO: Remove debugging
					result.set("debug", Json.toJson(created));
					return APIGlobal.response(true, title, "Serie was created successfuly", result, APIGlobal.SUCCESS);
				}
				catch (Exception e) {
				    //TODO: Remove debugging
					result.put("exception", e.getMessage());
					return APIGlobal.response(false, title, "Failed to create a new Serie", result, APIGlobal.ERROR_UNKNOWN);
				}
			}
		}
	//	result.put("test","test Works");
	//	result.set("js", json);
	//	return APIGlobal.response(true, title, "Serie was Created successfuly", result, APIGlobal.SUCCESS);
	}
	
	public Result serie_update(Integer sid) {

			ObjectNode result = Json.newObject();
		
			Serie serie = Serie.getbyid(sid);
			
			if (serie != null) {			
				JsonNode json = request().body().asJson();
				if (json == null) {
					play.Logger.error("Serie - serie_update - Expecting Json data!");
					return APIGlobal.response(false, title, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
				}
				else {
					ObjectNode object = (ObjectNode) json;
					
					if (object.hasNonNull("name")) {
						if (!Serie.is_name_available_for_update(object.get("name").asText(), sid)) {
							return APIGlobal.response(false, title, "Serie Name not available!", result, APIGlobal.ERROR_INPUT);
						}
					}

					try{
						Serie updated = Serie.updateSerie(sid, object);
						return APIGlobal.response(true, title, "Serie was updated successfuly", result, APIGlobal.SUCCESS);
					}
					catch (Exception e){
						result.put("exception", e.getMessage());
						return APIGlobal.response(false, title, "Failed to update a new Serie", result, APIGlobal.ERROR_UNKNOWN);
					}
				}
			}
			else {
				return APIGlobal.response(false, title, "Can't find Serie!", result, APIGlobal.ERROR_UNKNOWN);
			}

	}
	

}


