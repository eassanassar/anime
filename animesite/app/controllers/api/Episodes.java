package controllers.api;

import com.anime.global.APIGlobal;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class Episodes extends Controller {
	String title = "Episodes";
	public Result episode_create(){
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		if (json == null) {
			play.Logger.error("Episode - episode_create - Expecting Json data!");
			return APIGlobal.response(false, title, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
			//return APIGlobal.response(false, NAME, "Expecting Json data", result, APIGlobal.ERROR_INPUT);
		}

		result.put("test","test Works");
		result.set("js", json);
		return APIGlobal.response(true, title, "Episode was Created successfuly", result, APIGlobal.SUCCESS);
	}
	

}


