package controllers;

import play.data.Form;
import play.mvc.*;

import views.html.*;
import models.login.*;
/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class Auth extends Controller {
	public Result login() {
	    return ok(views.html.auth.login.render());
	}

	public Result register() {
	    return ok(views.html.auth.register.render());
	}
	
}

