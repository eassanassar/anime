package helpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.inject.Singleton;

import org.mindrot.jbcrypt.BCrypt;

import play.api.Play;
import play.libs.Crypto;

@Singleton
public class Passwords {
	public static String createPassword(String clearString) throws Exception {
	    if (clearString == null) {
	        throw new Exception("empty.password");
	    }
	    return BCrypt.hashpw(clearString, BCrypt.gensalt());
	}

	public static boolean checkPassword(String candidate, String encryptedPassword) {
	    if (candidate == null) {
	        return false;
	    }
	    if (encryptedPassword == null) {
	        return false;
	    }
	    return BCrypt.checkpw(candidate, encryptedPassword);
	}
	
	/*public static boolean isStrongPass(String password) {
		if (password.length() > 6) {
			return true;
		}
		return false;
	}*/
/*	
	public static String encryptString(String unencrypted) {
		if (unencrypted == null || unencrypted.isEmpty()) {
			return "";
		}
		return Play.current().injector().instanceOf(Crypto.class).encryptAES(unencrypted, AppConfig.encryptPassword);
	}
	
	public static String decryptString(String encrypted) {
		if (encrypted == null || encrypted.isEmpty()) {
			return "";
		}
		return Play.current().injector().instanceOf(Crypto.class).decryptAES(encrypted, AppConfig.encryptPassword);
	}
	
	private static String signToken(String token) {
		return Play.current().injector().instanceOf(Crypto.class).signToken(token);
	}
	
	private static String getDownloadToken(long timestamp, long uniqueid) {
		String downloadHash = Passwords.downloadHash(timestamp, uniqueid);
		return signToken(downloadHash);
	}
	
	private static String getUserLoginToken(long timestamp, String userName) {
		String userNameHash = Passwords.userLoginHash(timestamp, userName);
		return signToken(userNameHash);
	}
	
	private static boolean isDownloadTokenValid(long timestamp, long uniqueid, String token) {
		String signedToken = getDownloadToken(timestamp, uniqueid);
		
		if (Play.current().injector().instanceOf(Crypto.class).compareSignedTokens(signedToken, token)) {
			return true;
		}
		return false;
	}
	
	private static boolean isUserTokenValid(long timestamp, String userName, String token) {
		String signedToken = getUserLoginToken(timestamp, userName);
		
		if (Play.current().injector().instanceOf(Crypto.class).compareSignedTokens(signedToken, token)) {
			return true;
		}
		return false;
	}
	
	private static String downloadHash(long timestamp, long fileid) {
		return Long.toString(timestamp + (fileid * fileid)); // + Long.toString(fileid);
	}
	
	private static String userLoginHash(long timestamp, String userName) {
		return generateMD5(timestamp + userName + timestamp + "443"); //DON'T CHANGE
	}
	
	//TODO: Add option here to create a unique token for every hour, and not for each microsecond a different token
	public static SecureToken generateToken(long uniqueid) {
		Date date = new Date();
		long timestamp = date.getTime();
		String token = Passwords.getDownloadToken(timestamp, uniqueid);
		
		return new SecureToken(uniqueid, timestamp, token);
	}
	
	public static SecureToken generateTokenWithLogin(long uniqueid, String userName) {
		Date date = new Date();
		long mainTimestamp = date.getTime();
		String mainToken = Passwords.getDownloadToken(mainTimestamp, uniqueid);
		String userToken = Passwords.getUserLoginToken(mainTimestamp, userName);
		
		return new SecureToken(uniqueid, mainTimestamp, mainToken, userToken);
	}
	
	public static String generateMD5(String original) {
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("MD5");
			md.update(original.getBytes());
			byte[] digest = md.digest();
			
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
			
			return sb.toString();
		}
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	/**
	 * Checking Token for the request
	 * 
	 * @param timestamp 	- timestamp taken from url
	 * @param token			- Token taken from url
	 * @param uniqueid		- The fireid requested
	 * @return				- true/false if the token is valid and not expired
	 * 
	 * @throws Exception
	 */
	/*
	public static boolean checktoken(long timestamp, String token, long uniqueid) throws Exception {
		if (uniqueid <= 0) {
			throw new Exception("Not a valid request!");
		}
		
		//Check token if valid
		if (!Passwords.isDownloadTokenValid(timestamp, uniqueid, token)) {
			throw new Exception("URL not valid!");
		}
		
		
		Date currdate = new Date();
		Date LastdownloadTime = new Date(timestamp + (AppConfig.download_expire_hours * 3600 * 1000));
		
		if (currdate.after(LastdownloadTime)) {
			throw new Exception("Download has been expired!");
		}
		
		return true;
	}
	
	public static boolean checkUsertoken(long timestamp, String userToken, String userName) throws Exception {
		if (userName == null || userName.isEmpty()) {
			throw new Exception("User can't be empty!");
		}
		
		//Check token if valid
		if (!Passwords.isUserTokenValid(timestamp, userName, userToken)) {
			throw new Exception("User token not valid!");
		}
		
		Date currdate = new Date();
		Date LastdownloadTime = new Date(timestamp + (AppConfig.login_expire_minutes * 3600));
		
		if (currdate.after(LastdownloadTime)) {
			throw new Exception("User Login Token has been expired!");
		}
		
		return true;
	}
	
	public static long FilePath2Long(String str) {
		//We ignore these characters from the hashing so it won't damage the value
		str = str.replace("/", "");
		str = str.replace("\\", "");
		str = str.replace("%5C", "");
		str = str.replace("%2F", "");
		
		return String2Long(str) % 999999999;
	}
	
	private static long String2Long(String str) {
		long hash = 5; //DO NOT CHANGE THIS HASH NUMBER
		String md5Str = generateMD5(str);
		for (int i = 0; i < md5Str.length(); i++) {
		    hash = (hash * 31) + md5Str.charAt(i);
		}
		
		if (hash < 0) {
			hash = -hash;
		}
		
		return hash;
	}
	*/
}
