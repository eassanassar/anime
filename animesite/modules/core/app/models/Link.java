package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name= "links")
public class Link extends Model{
	
	@Id
	@Column(name = "id")
	private int id;
	
	@ManyToOne()
	@JoinColumn(name="episode_id", referencedColumnName="id")
	@JsonIgnore
	private Episode episode;
	
	@Column(name= "link")
	private String link;
	
	@Column(name = "dead")
	private boolean dead;

	@Column(name= "approved")
	private boolean approved;
	
	
	public int getId() {
		return id;
	}


	public Episode getEpisodeID() {
		return episode;
	}

	public void setEpisodeID(Episode episode) {
		this.episode = episode;
	}

	public String getLink() {
		return link;
	}

	public boolean isApproved() {
		return approved;
	}


	public void setApproved(boolean approved) {
		this.approved = approved;
	}


	public void setLink(String link) {
		this.link = link;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}
	

}
