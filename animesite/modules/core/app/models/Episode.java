package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "episode")
public class Episode extends Model{
	
	@Id
	@GeneratedValue
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="serie_id", referencedColumnName="id")
	@JsonIgnore
	private Serie serie;
	
	@Column(name= "episode")
	private int episode;
	
	@OneToMany(mappedBy = "episode", cascade = CascadeType.ALL)
	private List<Link> links = new ArrayList<Link>();

	private int season ;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getSeason() {
		return season;
	}
	
	public static Episode getbyid(long id) {
		return Ebean.find(Episode.class).where().eq("id", id).findUnique();	
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public int getEpisode() {
		return episode;
	}

	public void setEpisode(int episode) {
		this.episode = episode;
	}
	
	
	
	
}
