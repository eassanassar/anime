package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.avaje.ebean.Model;

@Entity
@Table(name="genre")
public class Genre extends Model {

	@Id
	@GeneratedValue
	private int id;
	
	@Column(name="name")
	private String name;
	
	//many to many
	@OneToMany(mappedBy = "genre", cascade = CascadeType.ALL)
	private List<serie2geners> series = new ArrayList<serie2geners>();
		

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<serie2geners> getSeries() {
		return series;
	}

	public void setSeries(List<serie2geners> series) {
		this.series = series;
	}

	
	
	
	/*
	@Column(name="action")
	private boolean action;
	
	@Column(name="animation")
	private boolean animation;
	
	@Column(name="adventure")
	private boolean adventure;

	@Column(name="bishounen ")
	private boolean bishounen ;
	
	@Column(name="comedy ")
	private boolean comedy ;
	
	@Column(name="demons")
	private boolean demons ;
	
	@Column(name="drama")
	private boolean drama;
	
	@Column(name="fantasy ")
	private boolean fantasy ;
	
	@Column(name="ecchi")
	private boolean ecchi;
	
	@Column(name="game")
	private boolean game;
	
	@Column(name="harem ")
	private boolean harem ;
	
	@Column(name="historical")
	private boolean historical;
	
	@Column(name="horror")
	private boolean horror;
	
	@Column(name="josei ")
	private boolean josei ;
	
	@Column(name="kids")
	private boolean kids;
	
	@Column(name="love")
	private boolean love; // love or romance 
	
	@Column(name="magic")
	private boolean magic;
	
	@Column(name="martial_arts")
	private boolean martial_arts;
	
	@Column(name="mecha")
	private boolean mecha ;
	
	@Column(name="military")
	private boolean military;
	
	@Column(name="music")
	private boolean music;
	
	@Column(name="mystery")
	private boolean mystery;
	
	@Column(name="police")
	private boolean police;
	
	@Column(name="samurai")
	private boolean samurai;
	
	@Column(name="school")
	private boolean school;
	
	@Column(name="sci_fi")
	private boolean sci_fi;
	
	@Column(name="seinen ")
	private boolean seinen;
	
	@Column(name="shoujo")
	private boolean shoujo;
	
	@Column(name="shoujo_ai")
	private boolean shoujo_ai;
	
	@Column(name="shounen")
	private boolean shounen;
	
	@Column(name="shounen_ai")
	private boolean shounen_ai;
	
	@Column(name="slice_of_life")
	private boolean slice_of_life;
	
	@Column(name="space")
	private boolean space;
	
	@Column(name="sports")
	private boolean sports;
	
	@Column(name="super_power")
	private boolean super_power;
	
	@Column(name="supernatural")
	private boolean supernatural;
	
	@Column(name="vampire")
	private boolean vampire;
	
	@Column(name="yaoi")
	private boolean yaoi;
	
	@Column(name="yuri")
	private boolean yuri;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isAction() {
		return action;
	}

	public void setAction(boolean action) {
		this.action = action;
	}

	public boolean isAnimation() {
		return animation;
	}

	public void setAnimation(boolean animation) {
		this.animation = animation;
	}

	public boolean isAdventure() {
		return adventure;
	}

	public void setAdventure(boolean adventure) {
		this.adventure = adventure;
	}

	public boolean isBishounen() {
		return bishounen;
	}

	public void setBishounen(boolean bishounen) {
		this.bishounen = bishounen;
	}

	public boolean isComedy() {
		return comedy;
	}

	public void setComedy(boolean comedy) {
		this.comedy = comedy;
	}

	public boolean isDemons() {
		return demons;
	}

	public void setDemons(boolean demons) {
		this.demons = demons;
	}

	public boolean isDrama() {
		return drama;
	}

	public void setDrama(boolean drama) {
		this.drama = drama;
	}

	public boolean isFantasy() {
		return fantasy;
	}

	public void setFantasy(boolean fantasy) {
		this.fantasy = fantasy;
	}

	public boolean isEcchi() {
		return ecchi;
	}

	public void setEcchi(boolean ecchi) {
		this.ecchi = ecchi;
	}

	public boolean isGame() {
		return game;
	}

	public void setGame(boolean game) {
		this.game = game;
	}

	public boolean isHarem() {
		return harem;
	}

	public void setHarem(boolean harem) {
		this.harem = harem;
	}

	public boolean isHistorical() {
		return historical;
	}

	public void setHistorical(boolean historical) {
		this.historical = historical;
	}

	public boolean isHorror() {
		return horror;
	}

	public void setHorror(boolean horror) {
		this.horror = horror;
	}

	public boolean isJosei() {
		return josei;
	}

	public void setJosei(boolean josei) {
		this.josei = josei;
	}

	public boolean isKids() {
		return kids;
	}

	public void setKids(boolean kids) {
		this.kids = kids;
	}

	public boolean isLove() {
		return love;
	}

	public void setLove(boolean love) {
		this.love = love;
	}

	public boolean isMagic() {
		return magic;
	}

	public void setMagic(boolean magic) {
		this.magic = magic;
	}

	public boolean isMartial_arts() {
		return martial_arts;
	}

	public void setMartial_arts(boolean martial_arts) {
		this.martial_arts = martial_arts;
	}

	public boolean isMecha() {
		return mecha;
	}

	public void setMecha(boolean mecha) {
		this.mecha = mecha;
	}

	public boolean isMilitary() {
		return military;
	}

	public void setMilitary(boolean military) {
		this.military = military;
	}

	public boolean isMusic() {
		return music;
	}

	public void setMusic(boolean music) {
		this.music = music;
	}

	public boolean isMystery() {
		return mystery;
	}

	public void setMystery(boolean mystery) {
		this.mystery = mystery;
	}

	public boolean isPolice() {
		return police;
	}

	public void setPolice(boolean police) {
		this.police = police;
	}

	public boolean isSamurai() {
		return samurai;
	}

	public void setSamurai(boolean samurai) {
		this.samurai = samurai;
	}

	public boolean isSchool() {
		return school;
	}

	public void setSchool(boolean school) {
		this.school = school;
	}

	public boolean isSci_fi() {
		return sci_fi;
	}

	public void setSci_fi(boolean sci_fi) {
		this.sci_fi = sci_fi;
	}

	public boolean isSeinen() {
		return seinen;
	}

	public void setSeinen(boolean seinen) {
		this.seinen = seinen;
	}

	public boolean isShoujo() {
		return shoujo;
	}

	public void setShoujo(boolean shoujo) {
		this.shoujo = shoujo;
	}

	public boolean isShoujo_ai() {
		return shoujo_ai;
	}

	public void setShoujo_ai(boolean shoujo_ai) {
		this.shoujo_ai = shoujo_ai;
	}

	public boolean isShounen() {
		return shounen;
	}

	public void setShounen(boolean shounen) {
		this.shounen = shounen;
	}

	public boolean isShounen_ai() {
		return shounen_ai;
	}

	public void setShounen_ai(boolean shounen_ai) {
		this.shounen_ai = shounen_ai;
	}

	public boolean isSlice_of_life() {
		return slice_of_life;
	}

	public void setSlice_of_life(boolean slice_of_life) {
		this.slice_of_life = slice_of_life;
	}

	public boolean isSpace() {
		return space;
	}

	public void setSpace(boolean space) {
		this.space = space;
	}

	public boolean isSports() {
		return sports;
	}

	public void setSports(boolean sports) {
		this.sports = sports;
	}

	public boolean isSuper_power() {
		return super_power;
	}

	public void setSuper_power(boolean super_power) {
		this.super_power = super_power;
	}

	public boolean isSupernatural() {
		return supernatural;
	}

	public void setSupernatural(boolean supernatural) {
		this.supernatural = supernatural;
	}

	public boolean isVampire() {
		return vampire;
	}

	public void setVampire(boolean vampire) {
		this.vampire = vampire;
	}

	public boolean isYaoi() {
		return yaoi;
	}

	public void setYaoi(boolean yaoi) {
		this.yaoi = yaoi;
	}

	public boolean isYuri() {
		return yuri;
	}

	public void setYuri(boolean yuri) {
		this.yuri = yuri;
	}
	
	
	*/
	
	
	

}
