package models;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Entity
@Table(name="serie")
public class Serie extends Model {
	@Id
	@GeneratedValue
	private long id;
	
	/*
	@OneToOne
    @JoinColumn(name="genre_id", referencedColumnName="id")
	private Genre genreId;
	*/
	@Column(name = "name", length=128)
	private String name;
	
	@Column(name = "summery", length=512)
	private String text;
	
	@Column(name = "released")
	private Timestamp released;
	
	@Column(name= "status")
	private String status;
	
	@OneToMany(mappedBy = "serie", cascade = CascadeType.ALL)
	private List<Episode> episodes = new ArrayList<Episode>();
	
	
	//many to many
	@OneToMany(mappedBy = "serie", cascade = CascadeType.ALL)
	private List<serie2geners> genres = new ArrayList<serie2geners>();
	
	@Version
	@UpdatedTimestamp
	private Timestamp updated;

	@JsonIgnore
	@CreatedTimestamp
	private Timestamp created;

	public Serie(){
		
	}
	
	public Serie(String name, String sum , Timestamp released ){
		this.name = name ;
		this.text = sum;
		this.released = released;
	}
	
	
	
	public List<serie2geners> getGenres() {
		return genres;
	}

	public void setGenres(List<serie2geners> genres) {
		this.genres = genres;
	}

	public long getId() {
		return id;
	}

	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}

	public void addEpisode(Episode  ep){
		episodes.add(ep);
	}
	

	public List<Episode> getEpisodes() {
		return episodes;
	}
	
	public List<Episode> getEpisodesList() {
		List<Episode> returnList = new ArrayList<Episode>();
		
		List<Episode> listall = this.getEpisodes();
		for (int i = 0; i< listall.size(); i++) {
			Episode contact = listall.get(i);
	
			returnList.add(contact);
		}
		
		return returnList;
	}



	public void setEpisodes(List<Episode> episodes) {
		this.episodes = episodes;
	}



	public static Serie getbyid(long id) {
		return Ebean.find(Serie.class).where().eq("id", id).findUnique();	
	}
	
	public static boolean is_name_available(String name) {
		ExpressionList<Serie> query = Ebean.find(Serie.class).where().eq("name", name);
		if (query.findRowCount() > 0) {
			return false;
		}
		return true;
	}
	
	
	public static boolean is_name_available_for_update(String name, long id) {
		ExpressionList<Serie> query = Ebean.find(Serie.class).where().eq("name", name).ne("id", id);
		if (query.findRowCount() > 0) {
			return false;
		}
		return true;
	}
	
	public static Serie addSerie(ObjectNode object) throws Exception {
		
		String name = object.get("name").asText();
		String sum = object.get("summery").asText();
		Timestamp released = Timestamp.valueOf(object.get("released").asText());
		
		Serie newSerie =  new Serie(name, sum, released);
		newSerie.save();

		return newSerie;
		
	}
	
	public static Serie updateSerie(Integer sid, ObjectNode object) {
		play.Logger.info("Settings - Updating Serie: "+sid+" ...");
		Serie dbs = getbyid(sid);
		
		//Mapping from ObjectNode
		try {
			//Add checking if properties exists
			
			if (object.hasNonNull("name")) {
				dbs.setName(object.get("name").asText());
			}
			if(object.hasNonNull("summery")){
				dbs.setText(object.get("summery").asText());
			}
			if(object.hasNonNull("released")){
				dbs.setReleased(Timestamp.valueOf(object.get("released").asText()));
			}
			
			dbs.save();
						
			play.Logger.info("Serie - Finished Updating Serie: "+sid+".");
		}
		catch (Exception e) {
			e.printStackTrace();
			play.Logger.error("Serie - Exception while updating Serie: "+sid+": "+e.getMessage());
		}
		
		return dbs;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Timestamp getReleased() {
		return released;
	}

	public void setReleased(Timestamp released) {
		this.released = released;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
}
