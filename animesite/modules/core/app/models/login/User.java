package models.login;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.Index;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import helpers.Passwords;
import play.Logger;
import play.data.validation.Constraints;


@Entity
@Table(name="user")
public class User extends Model{
	
	@Id
	@Column(name = "userid")
	private long id;
	
	@Column(name = "roleid")
	@JsonProperty("roleid")
	private int roleid;
	
	@Index
	@Column(length=64,nullable = true)
	private String username;
	
	@Index
	@Column(length=64, nullable=false)
	@Constraints.Required
	@Constraints.Email
	@JsonProperty("email")
	private String email;
	
	@Constraints.Required
	@Constraints.MinLength(6)
	@Constraints.MaxLength(64)
	@Column(length=128)
	@JsonIgnore
	private String password;
	
	
	@JsonIgnore
	private Timestamp lastlogin;
	
	@JsonIgnore
	@Version
	@UpdatedTimestamp
	private Timestamp updated;

	@JsonIgnore
	@CreatedTimestamp
	private Timestamp created;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@JsonIgnore
	@Column(name = "deleted")
	private boolean deleted = false;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		try{
			this.password = Passwords.createPassword(password);
		}catch(Exception e){
			Logger.error("User setPassword() - "+e.getMessage());
		}
	}

	public Timestamp getLastlogin() {
		return lastlogin;
	}

	public void setLastlogin(Timestamp lastlogin) {
		this.lastlogin = lastlogin;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public static boolean is_email_available(String email) {
		if (Ebean.find(User.class).where().eq("email", email).findRowCount() > 0) {
			return false;
		}
		return true;
	}
	
	public static User getuserbyemail(String email) {
		email = email.toLowerCase();
		ExpressionList<User> query = Ebean.find(User.class).where().eq("deleted", false).eq("email", email);
		
		if (query.findRowCount() > 0) {
			return query.findUnique();
		}
		return null;	
	}
	
	public static User getbyid(int l) {
		return Ebean.find(User.class, l);
	}
	
	
	public static User createEdit(JsonNode data, boolean isCreate) throws Exception{
		User user;
		if(isCreate){
			user = new User();
		}else{
			user = User.getbyid(data.get("id").asInt());
		}
		
		if(data != null){
			if(data.hasNonNull("email")){
				user.setEmail(data.get("email").asText().toLowerCase());
			}else{
				throw new Exception("email is not provided");
			}
			if(data.hasNonNull("password")){
				user.setPassword(data.get("password").asText());
			}
			user.save();
			
		}else{
			throw new Exception("No User Data were provided");
		}
		
		return user;
		
	}
	
	
}
