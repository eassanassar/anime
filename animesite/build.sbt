name := "animesite"

version := "1.0-SNAPSHOT"


lazy val commonSettings = Seq(
  organization := "com.eassa",
  version := "1.0.0",
  scalaVersion := "2.11.7"
)

lazy val Core = (project in file("modules/core"))
	.settings(commonSettings: _*)
    .settings(
        publishArtifact in (Compile, packageDoc) := false,
        publishArtifact in packageDoc := false,
        sources in (Compile,doc) := Seq.empty
    )
	.enablePlugins(PlayJava, PlayEbean)


lazy val root = (project in file("."))
.enablePlugins(PlayJava,SbtWeb)
.dependsOn(Core)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "mysql" % "mysql-connector-java" % "5.1.38"
)
