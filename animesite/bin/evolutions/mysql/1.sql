# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table episode (
  id                        integer auto_increment not null,
  serie_id                  integer,
  episode                   smallint,
  constraint pk_episode primary key (id))
;

create table anime_genre (
  id                        integer auto_increment not null,
  action                    tinyint(1) default 0,
  animation                 tinyint(1) default 0,
  adventure                 tinyint(1) default 0,
  bishounen                 tinyint(1) default 0,
  comedy                    tinyint(1) default 0,
  demons                    tinyint(1) default 0,
  drama                     tinyint(1) default 0,
  fantasy                   tinyint(1) default 0,
  ecchi                     tinyint(1) default 0,
  game                      tinyint(1) default 0,
  harem                     tinyint(1) default 0,
  historical                tinyint(1) default 0,
  horror                    tinyint(1) default 0,
  josei                     tinyint(1) default 0,
  kids                      tinyint(1) default 0,
  love                      tinyint(1) default 0,
  magic                     tinyint(1) default 0,
  martial_arts              tinyint(1) default 0,
  mecha                     tinyint(1) default 0,
  military                  tinyint(1) default 0,
  music                     tinyint(1) default 0,
  mystery                   tinyint(1) default 0,
  police                    tinyint(1) default 0,
  samurai                   tinyint(1) default 0,
  school                    tinyint(1) default 0,
  sci_fi                    tinyint(1) default 0,
  seinen                    tinyint(1) default 0,
  shoujo                    tinyint(1) default 0,
  shoujo_ai                 tinyint(1) default 0,
  shounen                   tinyint(1) default 0,
  shounen_ai                tinyint(1) default 0,
  slice_of_life             tinyint(1) default 0,
  space                     tinyint(1) default 0,
  sports                    tinyint(1) default 0,
  super_power               tinyint(1) default 0,
  supernatural              tinyint(1) default 0,
  vampire                   tinyint(1) default 0,
  yaoi                      tinyint(1) default 0,
  yuri                      tinyint(1) default 0,
  constraint pk_anime_genre primary key (id))
;

create table links (
  id                        integer auto_increment not null,
  episode_id                integer,
  link                      varchar(255),
  dead                      tinyint(1) default 0,
  constraint pk_links primary key (id))
;

create table serie (
  id                        integer auto_increment not null,
  genre_id                  integer,
  name                      varchar(128),
  summery                   varchar(512),
  released                  datetime(6),
  updated                   datetime(6) not null,
  created                   datetime(6) not null,
  constraint uq_serie_genre_id unique (genre_id),
  constraint pk_serie primary key (id))
;

create table user (
  userid                    bigint auto_increment not null,
  roleid                    integer,
  username                  varchar(64),
  email                     varchar(64) not null,
  password                  varchar(128),
  lastlogin                 datetime(6),
  updated                   datetime(6) not null,
  created                   datetime(6) not null,
  constraint pk_user primary key (userid))
;

alter table episode add constraint fk_episode_serie_1 foreign key (serie_id) references serie (id) on delete restrict on update restrict;
create index ix_episode_serie_1 on episode (serie_id);
alter table links add constraint fk_links_episodeID_2 foreign key (episode_id) references episode (id) on delete restrict on update restrict;
create index ix_links_episodeID_2 on links (episode_id);
alter table serie add constraint fk_serie_genreId_3 foreign key (genre_id) references anime_genre (id) on delete restrict on update restrict;
create index ix_serie_genreId_3 on serie (genre_id);


create index ix_user_username_4 on user(username);
create index ix_user_email_5 on user(email);

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table episode;

drop table anime_genre;

drop table links;

drop table serie;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

